//
//  Weather.swift
//  WeatherApplication
//
//  Created by Admin on 21.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class WeatherApi {
    
    private enum Method {
        case geocode(Double, Double)
        case conditions(String, String)
        case dailyForecast(String, String)
        case hourlyForecast(String, String)
    }
  
    private static let baseURLString = "http://api.wunderground.com/api/"
    private static let apiKey = "dfa83a285575c3c6/"
    private static let formata = ".json"
    private static let language = "lang:RU/"
    
    private static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyy-MM-dd HH:mm:ss"
        return formatter
    }()
    
    static func urlForImage(_ name: String) -> String {
        return "http://icons.wxug.com/i/c/i/\(name).gif"
    }
    
    static func urlForNightImage(_ name: String, hour: Int32) -> String {
        return "http://icons.wxug.com/i/c/i/\((hour > 19 || hour < 7) ? "nt_" : "")\(name).gif"
    }
    
    static func geoDataURL(forLatitude latitude: Double, forLongitude longitude: Double) -> URL? {
        return weatherURL(method: .geocode(latitude, longitude))
    }
    
    static func conditionsURL(forCity city: String, forCountry country: String) -> URL? {
        return weatherURL(method: .conditions(country, city))
    }
    static func dailyForecastURL(forCity city: String, forCountry country: String) -> URL? {
        return weatherURL(method: .dailyForecast(country, city))
    }
    
    static func hourlyForecastURL(forCity city: String, forCountry country: String) -> URL? {
        return weatherURL(method: .hourlyForecast(country, city))
    }
    
    private static func weatherURL(method: Method) -> URL? {
        var URLString = baseURLString + apiKey
        switch method {
        case let .geocode(lat, long):
            URLString += "geolookup/\(language)q/\(lat),\(long)"
        case let .conditions(country, city):
            URLString += "conditions/\(language)q/\(country)/\(removeSpaces(fromString: city))"
        case let .dailyForecast(country, city):
            URLString += "forecast10day/\(language)q/\(country)/\(removeSpaces(fromString: city))"
        case let .hourlyForecast(country, city):
            URLString += "hourly/\(language)q/\(country)/\(removeSpaces(fromString: city))"
        }
        URLString += formata
        return URL(string: URLString)
    }
    
    static func geoData(fromJSON data: Data) -> RequestResult {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            guard
                let location = (jsonObject as? Dictionary<String, Any>)?["location"] as? Dictionary<String, Any>,
                let nearby_weather_stations = location["nearby_weather_stations"] as? Dictionary<String, Any>,
                let stations = (nearby_weather_stations["pws"] as? Dictionary<String, Any>)?["station"] as? Array<Dictionary<String, Any>>,
                let base_city = location["city"] as? String,
                let base_country = location["country_iso3166"] as? String,
                let requestURL = location["requesturl"] as? String
                else {
                return .failure(RequestError.invalidJSONData)
            }
            var city = base_city
            var country = base_country
            if !stations.isEmpty {
                if let station_city = stations[0]["city"] as? String {
                    city = station_city
                }
                if let station_country = stations[0]["state"] as? String {
                    if station_country != "" {
                        country = station_country
                    }
                }
            }
            
            var resultDictionary = Dictionary<String, String>()
            resultDictionary["city"] = city
            resultDictionary["country"] = country
            resultDictionary["requesturl"] = requestURL
            return .success(resultDictionary)
        } catch let error {
            return .failure(error)
        }
    }
    
    static private func removeSpaces(fromString string: String) -> String {
        var charsArray = [Character]()
        for char in string.characters {
            if char != " " {
                charsArray.append(char)
            } else {
                charsArray.append("_")
            }
        }
        return String(charsArray)
    }
}
