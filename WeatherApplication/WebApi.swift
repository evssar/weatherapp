//
//  WebApi.swift
//  WeatherApplication
//
//  Created by Admin on 30.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import Alamofire

typealias DownloadComplete = (RequestResult) -> Void

class WebApi {
    static private let session: URLSession = {
        let config = URLSessionConfiguration.default
        return URLSession(configuration: config)
    }()
    
    static func fetchResult(forURL url: URL, withDataHandler handler: ((Data) -> RequestResult)?, withCallback callback: @escaping DownloadComplete) {
        print(url)
        Alamofire.request(url).responseData { response in
            var result: RequestResult
            switch response.result {
            case .success:
                if let data = response.result.value {
                    if let dataHandler = handler {
                        result = dataHandler(data)
                    } else {
                        result = RequestResult.success(data)
                    }
                } else {
                    print("Error fetching data: No data was fetched")
                    result = RequestResult.failure(RequestError.unexpectedError)
                }
            case .failure(let error):
                print("Error fetching data: \(error)")
                result = RequestResult.failure(error)
            }
            DispatchQueue.main.async { callback(result) }
        }
    }
}
