//
//  DiagrammaView.swift
//  CoreGraphicApplication
//
//  Created by Admin on 10.09.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreGraphics

class DiagrammaView: UIView {
    
    var title: String = "Temperature" { didSet { setNeedsDisplay() }}
    var lineWidth: CGFloat = 2.0 { didSet { setNeedsDisplay() }}
    var strokeWidth: CGFloat = 3.0 { didSet { setNeedsDisplay() }}
    var color: UIColor = UIColor.blue {didSet {setNeedsDisplay() }}
    var startColor: UIColor = UIColor(red: 250.0 / 255.0, green: 233.0 / 255.0, blue: 222.0 / 255.0, alpha: 1.0) { didSet { setNeedsDisplay() }}
    var endColor: UIColor = UIColor(red: 252.0 / 255.0, green: 79.0 / 255.0, blue: 8.0 / 255.0, alpha: 1.0) { didSet { setNeedsDisplay() }}
    var pointSize: CGFloat = 8.0 { didSet { setNeedsDisplay() }}
    var labelHeight: CGFloat = 15.0 { didSet { setNeedsDisplay() }}
    var labelWidth: CGFloat = 35.0 { didSet { setNeedsDisplay() }}
    var insets: UIEdgeInsets {
        set {
            setNeedsDisplay()
        }
        get {
            let size = ("+00" as NSString).size(attributes: stringAttributes)
            return UIEdgeInsets(top: size.height * 2 + strokeWidth, left: size.width, bottom: size.height + pointSize + strokeWidth, right: size.width)
        }
    }
    
    var fontSize: CGFloat = 16.0
    var fontName: String = "Impact"
    var paragraph: NSMutableParagraphStyle = {
        var paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center
        return paragraph
    }()
    
    var stringAttributes: [String : Any] {
        return [
            //NSStrokeWidthAttributeName : strokeWidth,
            //NSStrokeColorAttributeName : color,
            NSForegroundColorAttributeName : color,
            NSFontAttributeName : UIFont(name: fontName, size: fontSize) ?? UIFont.boldSystemFont(ofSize: fontSize),
            NSParagraphStyleAttributeName : paragraph
        ]
    }
    
    var dataArray: [Double] = [Double]() {
        didSet {
            clearAll()
            for (offset, _) in dataArray.enumerated() {
                _ = createPoint(indexedBy: offset)
            }
        }
    }
    
    var abscissaValues: [String] = [String]()
    
    var diagrammBehavior: DiagrammBehavior? {
        didSet {
            for (_ , point) in pointsDictionary {
                oldValue?.removePoint(point)
                diagrammBehavior?.addPoint(point)
            }
            diagrammBehavior?.action = {
                self.setNeedsDisplay()
            }
        }
    }
    
    private var pointsDictionary = [Int : DiagrammPointView]()
   
    private var boundingBox: CGRect {
        return CGRect(x: bounds.minX + insets.left,
                      y: bounds.minY + insets.top,
                      width: bounds.maxX - insets.right - insets.left,
                      height: bounds.maxY - insets.bottom - insets.top)
    }
    
    private var stepX: CGFloat {
        return (boundingBox.maxX - boundingBox.minX) / CGFloat((dataArray.count - 1))
    }
    
    private var scaleY: CGFloat {
        if let max = dataArray.max(), let min = dataArray.min() {
            return -(boundingBox.maxY - boundingBox.minY) / (CGFloat(max) - CGFloat(min))
        }
        return -1
    }

    private func setBoundaries() {
        for (index, point) in pointsDictionary {
            diagrammBehavior?.setBoundary(boundaryPathForPoint(point, at: index), named: "\(index)", handler: nil)
        }
    }
    
    private func boundaryPathForPoint(_ point: DiagrammPointView, at index: Int) -> UIBezierPath {
        
        let rect = CGRect(x: point.frame.minX, y: boundingBox.maxY + CGFloat(dataArray[index] - (dataArray.min() ?? 0.0)) * scaleY - 1.5 * pointSize, width: pointSize, height: pointSize)
        let path = UIBezierPath(rect: rect)
        return path
        
    }
    
    private func frameForLable(at index: Int, withSize size: CGSize) -> CGRect {
        if let point = pointsDictionary[index] {
            return CGRect(x: point.center.x - (index != 0 ? size.width : 0), y: point.frame.minY - size.height,  width: size.width, height: size.height)
        }
        return CGRect()
    }
    
    private func frameForAbscissLable(at index: Int, withSize size: CGSize) -> CGRect {
        if let point = pointsDictionary[index] {
            return CGRect(x: point.center.x - size.width / 2, y: boundingBox.maxY + pointSize,  width: size.width, height: size.height)
        }
        return CGRect()
    }
    
    private func valueForLabel(at index: Int) -> String {
        if let point = pointsDictionary[index] {
            let value = Double((point.center.y - boundingBox.maxY) / scaleY) + (dataArray.min() ?? 0.0)
            return String("\(value >= 0 ? "+" : "")\(Int(value) + 1)")
        }
        return String()
    }
    
    private func frameForGreedLine(withOrdinat ordinat: CGFloat, content: NSString) -> CGRect {
        let size = content.size(attributes: stringAttributes)
        return CGRect(x: bounds.maxX - size.width, y: ordinat - size.height / 2, width: size.width, height: size.height)
    }
    
    private func contentForGreedLine(forValue value: Double) -> NSString {
        return String("\(value >= 0 ? "+" : "")\(Int(value))") as NSString
    }
    
    private func createPoint(indexedBy index: Int) -> DiagrammPointView {
        let point: DiagrammPointView = DiagrammPointView()
        point.lineWidth = 0.0
        point.fillColor = color
        point.strokeColor = color
        point.isOpaque = false
        pointsDictionary[index] = point
        addSubview(point)
        let center: CGPoint = CGPoint(x: CGFloat(index) * stepX + boundingBox.minX, y: boundingBox.maxY)
        point.frame = CGRect(origin: CGPoint.zero, size: CGSize(width: pointSize, height: pointSize))
        point.center = center
        diagrammBehavior?.addPoint(point)
        return point
    }
    
    private func clearAll() {
        for (index, point) in pointsDictionary {
            point.removeFromSuperview()
            diagrammBehavior?.removePoint(point)
            pointsDictionary[index] = nil
        }
        diagrammBehavior?.removeAllBoundaries()
    }
    
    private func getGradient() -> CGGradient {
        let color = [startColor.cgColor, endColor.cgColor]
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        let colorLocation: [CGFloat] = [0.0, 1.0]
        return CGGradient(colorsSpace: colorSpace, colors: color as CFArray, locations: colorLocation)!
    }
    
    private func getMaxOrdinat() -> CGFloat {
        if let point = pointsDictionary.max(by: { $0.value.center.y < $1.value.center.y }) {
            return point.value.center.y
        }
        return 0.0
    }
    
    override func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        let gradient = getGradient()
        let startPoint = CGPoint.zero
        let endPoint = CGPoint(x: 0.0, y: bounds.height)
        
        let path = UIBezierPath(roundedRect: rect, cornerRadius: 8.0)
        path.addClip()
        context?.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: [])
        
        let linePath = UIBezierPath()
        linePath.lineJoinStyle = CGLineJoin.bevel
        linePath.move(to: pointsDictionary[0]?.center ?? CGPoint.zero)
        for (index, point) in pointsDictionary.sorted(by: { $0.key < $1.key }) {
            linePath.addLine(to: point.center)
            let pointLabel = valueForLabel(at: index) as NSString
            pointLabel.draw(with: frameForLable(at: index, withSize: pointLabel.size(attributes: stringAttributes)), options: .usesLineFragmentOrigin, attributes: stringAttributes, context: nil)
            if abscissaValues.count == dataArray.count  {
                let label = abscissaValues[index] as NSString
                label.draw(with: frameForAbscissLable(at: index, withSize: label.size(attributes: stringAttributes)), options: .usesLineFragmentOrigin, attributes: stringAttributes, context: nil)
            }
        }
        
        let greedLine = UIBezierPath()
        greedLine.move(to: boundingBox.origin)
        greedLine.addLine(to: CGPoint(x: boundingBox.maxX, y: boundingBox.minY))
        greedLine.move(to: CGPoint(x: boundingBox.minX, y: boundingBox.midY))
        greedLine.addLine(to: CGPoint(x: boundingBox.maxX, y: boundingBox.midY))
        greedLine.move(to: CGPoint(x: boundingBox.minX, y: boundingBox.maxY))
        greedLine.addLine(to: CGPoint(x: boundingBox.maxX, y: boundingBox.maxY))
        var label = NSString()
        if let min = dataArray.min(), let max = dataArray.max() {
            label = contentForGreedLine(forValue: max)
            label.draw(with: frameForGreedLine(withOrdinat: boundingBox.minY, content: label), options: .usesLineFragmentOrigin, attributes: stringAttributes, context: nil)
            let middleValue = min + (max - min) / 2
            label = contentForGreedLine(forValue: middleValue)
            label.draw(with: frameForGreedLine(withOrdinat: boundingBox.midY, content: label), options: .usesLineFragmentOrigin, attributes: stringAttributes, context: nil)
            label = contentForGreedLine(forValue: min)
            label.draw(with: frameForGreedLine(withOrdinat: boundingBox.maxY, content: label), options: .usesLineFragmentOrigin, attributes: stringAttributes, context: nil)
        }
        label = (title.uppercased()) as NSString
        label.draw(with: CGRect(x: 0.0, y: 0.0,  width: bounds.width, height: insets.top) , options: .usesLineFragmentOrigin, attributes: stringAttributes, context: nil)
        
        linePath.lineWidth = lineWidth
        
        color.setStroke()
        linePath.stroke()
        greedLine.stroke()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        reset()
        setBoundaries()
    }
    
    private func reset() {
        let array = dataArray
        dataArray = array
    }
}
