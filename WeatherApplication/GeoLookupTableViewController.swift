//
//  GeoLookupTableViewController.swift
//  WeatherApplication
//
//  Created by Admin on 31.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class GeoLookupTableViewController: UITableViewController {
    
    var locationsArray: Array<Place> = []
    var viewModel: ViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        view.addGradient()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return locationsArray.count
        default:
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "GeoLookupCell", for: indexPath)

        cell.textLabel?.text = locationsArray[indexPath.row].localName
        cell.detailTextLabel?.text = locationsArray[indexPath.row].localDescription

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.partiallyPlace.onNext(locationsArray[indexPath.row])
        if let nc = navigationController {
            if nc.viewControllers.count >= 2 {
                nc.popViewController(animated: true)
            }
        }
    }
}
