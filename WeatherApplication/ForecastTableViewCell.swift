//
//  ForecastTableViewCell.swift
//  WeatherApplication
//
//  Created by Admin on 20.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Kingfisher

class ForecastTableViewCell: UITableViewCell {

    @IBOutlet weak var forecastImage: UIImageView!
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    var forecast: DailyForecastItem? {
        didSet {
            UpdateUI()
        }
    }
    
    private func UpdateUI() {
        weekdayLabel.text = forecast?.weekday
        descriptionLabel.text = forecast?.desc
        if let url = URL(string: (forecast?.icon_url)!) {
            forecastImage.kf.setImage(with: url)
        }
        temperatureLabel.text = forecast?.temperatureInterval
        dateLabel.text = forecast?.date
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
