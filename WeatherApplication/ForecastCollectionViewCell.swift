//
//  ForecastCollectionViewCell.swift
//  WeatherApplicationForecastCollectionViewControlelr
//
//  Created by Admin on 09.09.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import Kingfisher

class ForecastCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var forecastImage: UIImageView!
 
    var forecast: HourlyForecastItem? {
        didSet {
            UpdateUI()
        }
    }
    
    private func UpdateUI() {
        weekdayLabel.text = "\(forecast?.weekday_name_abbrev ?? "").\(forecast?.date.uppercased() ?? "")"
        temperatureLabel.text = forecast?.temp
        if let url = URL(string: (forecast?.icon_url)!) {
            forecastImage.kf.setImage(with: url)
        }
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        let height = bounds.height / 3
        let imageSize = CGSize(width: height, height: height)
        forecastImage.frame = CGRect(origin: CGPoint.zero, size: imageSize)
        forecastImage.center = CGPoint(x: bounds.midX, y: bounds.midY)
        weekdayLabel.frame = bounds.divided(atDistance: height, from: CGRectEdge.minYEdge).slice
        temperatureLabel.frame = bounds.divided(atDistance: height, from: CGRectEdge.maxYEdge).slice
    }
}
