//
//  Place.swift
//  WeatherApplication
//
//  Created by Admin on 30.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class Place: NSManagedObject, Mappable {
    /*
    public var longitude: Double = 0.0
    public var latitude: Double = 0.0
    public var localName: String = ""
    public var localDescription = ""
    public var city = ""
    public var country = ""
    public var requestURL = ""
    */
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        localName <- map["GeoObject.name"]
        localDescription <- map["GeoObject.description"]
        longitude <- (map["GeoObject.Point.pos"], TransformOf<Double, String>(
            fromJSON:{ (value: String?) -> Double? in return self.get(coord: .longitude, from: value!) }, toJSON: self.getJSON))
        latitude <- (map["GeoObject.Point.pos"], TransformOf<Double, String>(
            fromJSON:{ (value: String?) -> Double? in return self.get(coord: .latitude, from: value!) }, toJSON: self.getJSON))
    }
    
    func placeToStruct() -> sPlace {
        var place = sPlace()
        place.longitude = longitude
        place.latitude = latitude
        place.localName = localName ?? ""
        place.localDescription = localDescription ?? ""
        place.city = city ?? ""
        place.country = country ?? ""
        place.requestURL = requestURL ?? ""
        return place
    }
    
    static func placeFromStruct(_ struc: sPlace, context: NSManagedObjectContext) -> Place {
        let place = Place(context: context)
        place.longitude = struc.longitude
        place.latitude = struc.latitude
        place.localName = struc.localName
        place.localDescription = struc.localDescription
        place.city = struc.city
        place.country = struc.country
        place.requestURL = struc.requestURL
        return place
    }
    
    private enum Coords {
        case latitude
        case longitude
    }
    
    private func get(coord: Coords, from string: String) -> Double? {
        var coordArray = string.characters.split(separator: " ")
        if coordArray.count != 2 { return 0.0 }
        switch coord {
        case .latitude:
            return Double(String(coordArray[1]))
        case .longitude:
            return Double(String(coordArray[0]))
        }
    }
    
    private func getJSON(fromDouble number: Double?) -> String? {
        return String(describing: number)
    }
    
}

struct sPlace {
    var longitude: Double = 0.0
    var latitude: Double = 0.0
    var localName: String = ""
    var localDescription = ""
    var city = ""
    var country = ""
    var requestURL = ""
}
