//
//  ViewModel.swift
//  WeatherApplication
//
//  Created by Admin on 08.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import CoreLocation
import RxSwift


class ViewModel: NSObject, CLLocationManagerDelegate {
    let disposeBag = DisposeBag()
    var location = PublishSubject<CLLocation?>()
    var places = PublishSubject<[Place]?>()
    var partiallyPlace = PublishSubject<Place?>()
    
    var place = BehaviorSubject<Place?>(value: nil)
    var weather = BehaviorSubject<Weather?>(value: nil)
    var isOnline = BehaviorSubject<Bool>(value: true)
    var dataReady = BehaviorSubject<Bool>(value: false)
    
    var errorBuffer = PublishSubject<Error>()
    var searchText = PublishSubject<String>()
    var startPolling = PublishSubject<Bool>()
    
    //var hourlyForecast = PublishSubject<[HourlyForecastItem]?>()
    //var dailyForecast = PublishSubject<[DailyForecastItem]?>()
    
    private var successCounter: Int = 0 {
        didSet {
            if successCounter > 2 {
                dataReady.onNext(true)
                successCounter = 0
            }
        }
    }
    
    var locationManager: CLLocationManager = {
        let lm = CLLocationManager()
        lm.desiredAccuracy = kCLLocationAccuracyBest
        lm.distanceFilter = 1000.0
        return lm
    }()
    
    override init() {
        super.init()
        
        locationManager.delegate = self
        
        startPolling.subscribe(onNext: { isCheck in
            if isCheck {
                if self.checkStatus() {
                    self.startUpdateLocation()
                }
            } else {
                if self.checkServices() {
                    self.startUpdateLocation()
                }
            }
        }).addDisposableTo(disposeBag)
        
        location.subscribe(onNext: { position in
            if let position = position {
                if let url = GeoLookupApi.geocodeURL(withLatitude: position.coordinate.latitude, withLongitude: position.coordinate.longitude) {
                    WebApi.fetchResult(forURL: url, withDataHandler: DatabaseWorker.places, withCallback: self.geoLookupCallback)
                }
            }
        }).addDisposableTo(disposeBag)
        
        searchText.subscribe(onNext: { searchText in
            if let url = GeoLookupApi.geocodeURL(forName: searchText) {
                if self.checkInternet() {
                    WebApi.fetchResult(forURL: url, withDataHandler: DatabaseWorker.places, withCallback: self.geoLookupCallback)
                } else {
                    
                }
            }
        }).addDisposableTo(disposeBag)
        partiallyPlace.subscribe(onNext: { currentPlace in
            self.fetchGeoData(for: currentPlace)
        }).addDisposableTo(disposeBag)
        
        place.subscribe(onNext: { currentPlace in
            if let currentPlace = currentPlace {
                self.fetchCurrentWeather(for: currentPlace)
            }
        }).addDisposableTo(disposeBag)
        
        place.subscribe(onNext: { currentPlace in
            if let currentPlace = currentPlace {
                self.fetchHourlyForecast(for: currentPlace)
            }
        }).addDisposableTo(disposeBag)
        
        place.subscribe(onNext: { currentPlace in
            if let currentPlace = currentPlace {
                self.fetchDailyForecast(for: currentPlace)
            }
        }).addDisposableTo(disposeBag)
    }
    
    private func checkStatus() -> Bool {
        let (result, weather) = isDatabaseHaveData()
        if result {
            if !isDataOlderThen(weather!, minutes: 1) {
                dataReady.onNext(true)
                return false
            } else {
                if checkLocationService() && checkInternet() {
                    return true
                } else {
                    isOnline.onNext(false)
                    errorBuffer.onNext(RequestError.dataOlderThenExpected)
                    return false
                }
            }
        }
        return checkServices()
    }
    
    private func checkInternet() -> Bool {
        let result = CheckInternet.isConnectedToNetwork()
        if !result {
            isOnline.onNext(false)
        }
        isOnline.onNext(true)
        return result
    }
    
    private func checkLocationService() -> Bool {
        if CLLocationManager.locationServicesEnabled() {
            let status = CLLocationManager.authorizationStatus()
            switch status {
            case .authorizedAlways, .authorizedWhenInUse, .notDetermined:
                return true
            default:
                return false
            }
        }
        return false
    }
    
    private func checkServices() -> Bool {
        if checkLocationService() {
            if checkInternet() {
                return true
            } else {
                errorBuffer.onNext(RequestError.isNotConnectedToNetwork)
                return false
            }
        }
        errorBuffer.onNext(RequestError.locationIsProhibited)
        return false
    }
    
    private func isDatabaseHaveData() -> (Bool, Weather?) {
        let result = DatabaseWorker.getWeather()
        switch  result {
        case .success(let data):
            if let weather = data as? Weather {
                return (true, weather)
            }
        case .failure(_):
            break
        }
        return (false, nil)
    }
    
    private func isDataOlderThen(_ weather: Weather, minutes: Double) -> Bool {
        if let date = weather.createdAt {
            if Date().timeIntervalSince(date as Date) > minutes * 60 {
                return true
            }
        }
        return false
    }

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            locationManager.startUpdatingLocation()
        default:
            break
        }
    }
 
    private var allowedUpdatedLocation = false
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if allowedUpdatedLocation {
            location.onNext(locationManager.location)
            locationManager.stopUpdatingLocation()
            allowedUpdatedLocation = false
        }
    }
    
    private func startUpdateLocation() {
        let status = CLLocationManager.authorizationStatus()
         allowedUpdatedLocation = true
        switch status {
        case .authorizedWhenInUse, .authorizedAlways:
            locationManager.startUpdatingLocation()
        case .notDetermined:
            locationManager.requestWhenInUseAuthorization()
        default:
            errorBuffer.onNext(RequestError.locationIsProhibited)
        }
    }
    
    private func geoLookupCallback(result: RequestResult) {
        switch result {
        case let .success(array):
            if let places = array as? [Place] {
                if !places.isEmpty {
                    self.places.onNext(places)
                }
            }
        default:
            errorBuffer.onNext(RequestError.invalidJSONData)
        }
    }
    
    private func fetchGeoData(for place: Place?) {
        if place != nil {
            if let url = WeatherApi.geoDataURL(forLatitude: place!.latitude, forLongitude: place!.longitude) {
                WebApi.fetchResult(forURL: url, withDataHandler: WeatherApi.geoData)
                {(result: RequestResult) -> Void in
                    switch result {
                    case let .success(value):
                        guard
                            let resultDictionary = value as? Dictionary<String, String>,
                            let city = resultDictionary["city"],
                            let country = resultDictionary["country"],
                            let requestURL = resultDictionary["requesturl"]
                            else {
                                self.errorBuffer.onNext(RequestError.unexpectedError)
                                return
                        }
                        place?.city = city
                        place?.country = country
                        place?.requestURL = requestURL
                        
                        let sPlc = place?.placeToStruct() ?? sPlace()
                        DatabaseWorker.clearContext()
                        let result = DatabaseWorker.getPlaceFromStruct(sPlc)
                        switch result {
                        case .success(let data):
                            if let classPlace = data as? Place {
                                self.place.onNext(classPlace)
                            }
                        case .failure(let error):
                            self.errorBuffer.onNext(error)
                        }
                    case .failure(let error):
                        self.errorBuffer.onNext(error)
                    }
                }
            }
        }
    }
    
    private func fetchCurrentWeather(for place: Place?) {
        if  let location = place {
            if let url = WeatherApi.conditionsURL(forCity: location.city ?? "", forCountry: location.country ?? "") {
                WebApi.fetchResult(forURL: url, withDataHandler: DatabaseWorker.conditions(fromJSON:))
                {(result: RequestResult) -> Void in
                    switch result {
                    case .success(let data):
                        guard
                            let weather = data as? Weather
                            else {
                                self.errorBuffer.onNext(RequestError.unexpectedError)
                                return
                        }
                        self.weather.onNext(weather)
                        self.successCounter += 1

                    case .failure(let error):
                        self.errorBuffer.onNext(error)
                    }
                }
            }
        }
    }
    
    private func fetchDailyForecast(for place: Place?) {
        if  let location = place {
            if let url = WeatherApi.dailyForecastURL(forCity: location.city ?? "", forCountry: location.country ?? "") {
                print("fetchDailyForecastStart")
                WebApi.fetchResult(forURL: url, withDataHandler: DatabaseWorker.dailyForecast(fromJSON:))
                {(result) in
                    switch result {
                    case .success(_):
                        self.successCounter += 1
                    case .failure(let error):
                        self.errorBuffer.onNext(error)
                    }
                }
            }
        }
    }
    
    private func fetchHourlyForecast(for place: Place?) {
        if  let location = place {
            if let url = WeatherApi.hourlyForecastURL(forCity: location.city ?? "", forCountry: location.country ?? "") {
                WebApi.fetchResult(forURL: url, withDataHandler: DatabaseWorker.hourlyForecast(fromJSON:)) {(result) in
                    switch result {
                    case .success(_):
                        /*if let array = data as? Array<HourlyForecastItem> {
                            
                        } else {
                            self.errorBuffer.onNext(RequestError.unexpectedError)
                            return
                        }*/
                        self.successCounter += 1
                    case .failure(let error):
                        self.errorBuffer.onNext(error)
                    }
                }
            }
        }
    }
}
