//
//  Weather.swift
//  WeatherApplication
//
//  Created by Admin on 03.08.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class Weather : NSManagedObject, Mappable {
    var icon_url: String {
        return WeatherApi.urlForNightImage(icon ?? "", hour: Int32(hour))
    }

    fileprivate var _date: String!
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateStyle = .long
        dateFormatter.timeStyle = .none
        let currentDate = dateFormatter.string(from: Date())
        self._date  = currentDate
        return _date
    }
    
    var weekday: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: Date())
    }
    
    var hour: Int {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "HH"
        return Int(dateFormatter.string(from: Date())) ?? 0
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let transformOfIntToInt32 = TransformOf<Int32, Int>(fromJSON: IntToInt32, toJSON: Int32ToJSON)
        weather_type <- map["weather"]
        temp_c <- (map["temp_c"], transformOfIntToInt32)
        wind_dir <- map["wind_dir"]
        wind_kmh <- (map["wind_kph"], transformOfIntToInt32)
        pressure_mb <- map["pressure_mb"]
        feelslike_c <- (map["feelslike_c"], transformOfIntToInt32)
        icon <- map["icon"]
        
        createdAt = Date() as NSDate
    }
    
    private func Int32ToJSON(number: Int32?) -> Int? {
        return Int(number ?? 0)
    }
    
    private func IntToInt32(int: Int?) -> Int32? {
        return Int32(int ?? 0)
        
    }

}
