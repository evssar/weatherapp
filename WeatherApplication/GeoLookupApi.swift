//
//  GeoLookup.swift
//  WeatherApplication
//
//  Created by Admin on 21.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import ObjectMapper

class GeoLookupApi {

    private enum Method : String {
        case geocode = "geocode"
    }
    
    static private let baseURLString = "https://geocode-maps.yandex.ru/1.x"
    static private let apiKey = "AK9fclkBAAAAoEslRAMAeJQ-rWQJ48VAlNNC8gCd_vQfmG0AAAAAAAAAAAAHhZTJiBbor7ZV__N3nNep9uEnFA=="
    static private let baseParameters = [
        "key" : apiKey,
        "format" : "json",
        "lang" : "ru_RU",
        ]

    static public func geocodeURL(forName searchString: String) -> URL? {
        return geoLookupURL(method: .geocode, methodValue: searchString, parameters: nil)
    }
    
    static public func geocodeURL(withLatitude latitude: Double, withLongitude longitude: Double) -> URL? {
        return geoLookupURL(method: .geocode, methodValue: String("\(longitude),\(latitude)"), parameters: nil)
    }

    private static func geoLookupURL(method: Method, methodValue: String, parameters: Dictionary<String, String>?) -> URL? {
        var components = URLComponents(string: baseURLString)
        var queryItems = Array<URLQueryItem>()
        queryItems.append(URLQueryItem(name: method.rawValue, value: methodValue))
        for (key, value) in baseParameters {
            let item = URLQueryItem(name: key, value: value)
            queryItems.append(item)
        }
        if let additionalParameters = parameters {
            for (key, value) in additionalParameters {
                let item = URLQueryItem(name: key, value: value)
                queryItems.append(item)
            }
        }
        components?.queryItems = queryItems
        
        return components?.url
    }
}
