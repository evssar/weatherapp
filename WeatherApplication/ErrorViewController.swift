//
//  ErrorViewController.swift
//  WeatherApplication
//
//  Created by Admin on 09.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class ErrorViewController: UIViewController {

    @IBOutlet weak var errorMessageLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    @IBAction func actionButtonPush(_ sender: Any) {
        if let action = action {
            action()
            if let nc = navigationController {
                if nc.viewControllers.count >= 2 {
                    nc.popViewController(animated: true)
                }
            }
        }
    }
    
    private var action: (() -> Void)?
    private var buttonTitle: String?
    var message: String?
    
    func setAction(withTitle title: String, action: @escaping (() -> Void)) {
        buttonTitle = title
        self.action = action
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if action != nil {
            actionButton.isHidden = false
            actionButton.setTitle(buttonTitle ?? "", for: .normal)
        } else {
            actionButton.isHidden = true
        }
        errorMessageLabel.text = message ?? ""
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 }
