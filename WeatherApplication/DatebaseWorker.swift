//
//  DatebaseWorker.swift
//  WeatherApplication
//
//  Created by Admin on 07.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class DatabaseWorker {
    static let context = AppDelegate.viewContext
    
    static func getDailyForecast() -> RequestResult {
        let request: NSFetchRequest<DailyForecastItem> = DailyForecastItem.fetchRequest()
        request.sortDescriptors = [
            NSSortDescriptor(key: "period", ascending: true)
        ]
        do {
            let result = try context.fetch(request)
            return .success(result)
        } catch let error {
            return .failure(error)
        }
    }

    static func getHourlyForecast() -> RequestResult {
        let request: NSFetchRequest<HourlyForecastItem> = HourlyForecastItem.fetchRequest()
        request.sortDescriptors = [
            NSSortDescriptor(key: "year", ascending: true),
            NSSortDescriptor(key: "yday", ascending: true),
            NSSortDescriptor(key: "hour", ascending: true)
        ]
        do {
            let result = try context.fetch(request)
            return .success(result)
        } catch let error {
            return .failure(error)
        }
    }
    
    static func getWeather() -> RequestResult {
        let request: NSFetchRequest<Weather> = Weather.fetchRequest()
        do {
            let result = try context.fetch(request)
            if result.count > 0 {
                return .success(result[0])
            } else {
                return .failure(RequestError.unexpectedError)
            }
        } catch let error {
            return .failure(error)
        }
    }
    
    static func getPlace() -> RequestResult {
        let request: NSFetchRequest<Place> = Place.fetchRequest()
        do {
            let result = try context.fetch(request)
            if result.count > 0 {
                print(result.count)
                return .success(result[0])
            } else {
                return .failure(RequestError.unexpectedError)
            }
        } catch let error {
            return .failure(error)
        }
    }
    
    static func getAllPlaces() -> RequestResult {
        let request: NSFetchRequest<Place> = Place.fetchRequest()
        do {
            let result = try context.fetch(request)
            return .success(result)
        } catch let error {
            return .failure(error)
        }
    }
    
    static func getAllWeather() -> RequestResult {
        let request: NSFetchRequest<Weather> = Weather.fetchRequest()
        do {
            let result = try context.fetch(request)
            return .success(result)
        } catch let error {
            return .failure(error)
        }
    }
    
    
    static func clearContext() {
        _ = clearEntity(result: getDailyForecast())
        _ = clearEntity(result: getHourlyForecast())
        _ = clearEntity(result: getAllWeather())
        _ = clearEntity(result: getWeather())
    }
    
    static private func clearEntity(result: RequestResult) -> RequestResult {
        switch result {
        case .success(let items):
            if let items = items as? [NSManagedObject] {
                for item in items {
                    context.delete(item)
                }
            }
        default:
            return .failure(RequestError.databaseError)
        }
        do {
            try context.save()
            return .success(true)
        } catch let error {
            return .failure(error)
        }
    }
    
    static func getPlaceFromStruct(_ place: sPlace) -> RequestResult {
        let place = Place.placeFromStruct(place, context: context)
        do {
            try context.save()
            return .success(place)
        } catch let error {
            return .failure(error)
        }
        
    }
    
    static func conditions(fromJSON data: Data) -> RequestResult {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            guard
                let data = (jsonObject as? Dictionary<String, Any>)?["current_observation"] as? Dictionary<String, Any>
                else {
                    return .failure(RequestError.invalidJSONData)
            }
            let weather = Weather(context: context)
            let map = Map(mappingType: .fromJSON, JSON: data)
            weather.mapping(map: map)
            do {
                try context.save()
            }
            catch let error {
                return .failure(error)
            }

            return .success(weather)
        } catch let error {
            return .failure(error)
        }
    }
    
    static func dailyForecast(fromJSON data: Data) -> RequestResult {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            guard
                let forecast = (jsonObject as? Dictionary<String, Any>)?["forecast"] as? Dictionary<String, Any>,
                let txt_forecast = (forecast["txt_forecast"] as? Dictionary<String, Any>)?["forecastday"] as? Array<Dictionary<String, Any>>,
                let simpleforecast = (forecast["simpleforecast"] as? Dictionary<String, Any>)?["forecastday"] as? Array<Dictionary<String, Any>>
                else {
                    return .failure(RequestError.invalidJSONData)
            }
            var result = Array<DailyForecastItem>()
            if !txt_forecast.isEmpty && !simpleforecast.isEmpty {
                for i in 0 ..< txt_forecast.count {
                    let forecastItem = DailyForecastItem(context: context)
                    let txt_map: Map = Map(mappingType: .fromJSON, JSON: txt_forecast[i])
                    let simple_map: Map = Map(mappingType: .fromJSON, JSON: simpleforecast[i/2])
                    forecastItem.mappingTxt(map: txt_map)
                    forecastItem.mappingSimple(map: simple_map)
                    result.append(forecastItem)
                }
                do {
                    try context.save()
                }
                catch let error {
                    return .failure(error)
                }
            }
            if result.isEmpty {
                return .failure(RequestError.invalidJSONData)
            }
            return .success(result)
        } catch let error {
            return .failure(error)
        }
    }
    
    static func hourlyForecast(fromJSON data: Data) -> RequestResult {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            guard
                let forecast = (jsonObject as? Dictionary<String, Any>)?["hourly_forecast"] as? [Dictionary<String, Any>]
                else {
                    return .failure(RequestError.invalidJSONData)
            }
            var result = Array<HourlyForecastItem>()
            if !forecast.isEmpty {
                for i in 0 ..< forecast.count {
                    let forecastItem = HourlyForecastItem(context: context)
                    let map = Map(mappingType: .fromJSON, JSON: forecast[i])
                    forecastItem.mapping(map: map)
                    context.insert(forecastItem)
                    result.append(forecastItem)
                }
                do {
                    try context.save()
                }
                catch let error {
                    return .failure(error)
                }
            }
            
            if result.isEmpty {
                return .failure(RequestError.invalidJSONData)
            }
            return .success(result)
        } catch let error {
            return .failure(error)
        }
    }
    
    static func places(fromJSON data: Data) -> RequestResult {
        do {
            let jsonObject = try JSONSerialization.jsonObject(with: data, options: [])
            guard
                let getObjectCollection = ((jsonObject as? Dictionary<String, Any>)?["response"] as? Dictionary<String, Any>)?["GeoObjectCollection"] as? Dictionary<String, Any>,
                let placesArray = getObjectCollection["featureMember"] as? Array<Dictionary<String, Any>>
                
                else {
                    return .failure(RequestError.invalidJSONData)
            }
            var places = Array<Place>()
            
            for placeJSON in placesArray {
                let currentPlace = Place(context: context)
                let map = Map(mappingType: .fromJSON, JSON: placeJSON)
                currentPlace.mapping(map: map)
                places.append(currentPlace)
            }
            do {
                try context.save()
            }
            catch let error {
                return .failure(error)
            }
            
            if places.isEmpty && !placesArray.isEmpty {
                return .failure(RequestError.invalidJSONData)
            }
            return .success(places)
        } catch let error {
            return .failure(error)
        }
    }
}
