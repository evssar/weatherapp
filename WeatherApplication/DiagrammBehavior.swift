//
//  DiagrammBehavior.swift
//  DynamicAnimation
//
//  Created by Admin on 11.09.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class DiagrammBehavior: UIDynamicBehavior, UICollisionBehaviorDelegate
{
    private var points = [DiagrammPointView]()
    private var collisionHandlers = [String : (Void) -> Void]()

    private lazy var collider: UICollisionBehavior = {
        let behavior = UICollisionBehavior()
        behavior.collisionMode  = .everything
        behavior.collisionDelegate = self
        return behavior
    }()
    
    private lazy var physics: UIDynamicItemBehavior = {
        let behavior = UIDynamicItemBehavior()
        behavior.elasticity = 0.5
        behavior.allowsRotation = false
        behavior.friction = 0
        behavior.resistance = 0
        return behavior
    }()
    
    lazy var acceleration: UIGravityBehavior = {
        let behavior = UIGravityBehavior()
        behavior.magnitude = 0
        return behavior
    }()
  
    func setBoundary(_ path: UIBezierPath?, named name: String, handler: ((Void) -> Void)?) {
        collider.removeBoundary(withIdentifier: name as NSString)
        collisionHandlers[name] = nil
        
        if path != nil {
            collider.addBoundary(withIdentifier: name as NSString, for: path!)
            collisionHandlers[name] = handler
        }
    }
    
    func removeAllBoundaries() {
        collider.removeAllBoundaries()
    }
    
    func addPoint(_ point: DiagrammPointView) {
        points.append(point)
        collider.addItem(point)
        physics.addItem(point)
        acceleration.addItem(point)
    }
    
    func removePoint(_ point: DiagrammPointView) {
        if let index = points.index(of: point) {
            points.remove(at: index)
            collider.removeItem(point)
            physics.removeItem(point)
            acceleration.removeItem(point)
        }
    }
    
    override init() {
        super.init()
        addChildBehavior(collider)
        addChildBehavior(physics)
        addChildBehavior(acceleration)
    }
  
    // UICollisionBehaviorDelegate implementation
    
    func collisionBehavior(_ behavior: UICollisionBehavior, beganContactFor item: UIDynamicItem, withBoundaryIdentifier identifier: NSCopying?, at p: CGPoint) {
        if let name = identifier as? String, let handler = collisionHandlers[name] {
            handler()
        }
    }
}
