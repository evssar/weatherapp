//
//  ViewController.swift
//  DynamicAnimation
//
//  Created by Admin on 11.09.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreData

class GraphViewController: UIViewController {

    private var itemsSet: [DailyForecastItem]?
    private var lowTempButton: UIButton!
    private var highTempButton: UIButton!
    private var diagrammView: DiagrammaView!
    private var diagrammBehavior = DiagrammBehavior()
    private lazy var animator: UIDynamicAnimator = UIDynamicAnimator(referenceView: self.diagrammView)

    override func viewDidLoad() {
        super.viewDidLoad()
        let result = DatabaseWorker.getDailyForecast()
        switch result {
        case .success(let items):
            itemsSet = items as? [DailyForecastItem]
        case .failure(let error):
            print(error)
        }
        lowTempButton = createButton(withTitel: "LowTemp", withColor: UIColor.blue)
        highTempButton = createButton(withTitel: "HighTemp", withColor: UIColor.red)
        
        self.view.addSubview(lowTempButton)
        self.view.addSubview(highTempButton)
        
        lowTempButton.addTarget(self, action: #selector(self.lowTempButtonAction), for: UIControlEvents.touchDown)
        highTempButton.addTarget(self, action: #selector(self.highTempButtonAction), for: UIControlEvents.touchDown)
        
        view.addGradient()
    }
    
    private func createButton(withTitel title: String, withColor color: UIColor) -> UIButton {
        let button = UIButton()
        button.backgroundColor = UIColor.clear
        button.layer.cornerRadius = 5
        button.layer.borderColor = color.cgColor
        button.layer.borderWidth = 2.0
        button.contentEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
        button.setTitleColor(color, for: .normal)
        button.setTitle(title, for: .normal)
        return button
    }
    
    func lowTempButtonAction(_ sender: UIButton) {
        if let items = itemsSet {
            var values = Array<Double>()
            var keys = Array<String>()
            for (index, item) in items.enumerated() {
                if (index % 2) != 0 {
                    values.append(Double(item.low_temp))
                    keys.append(item.weekday_short ?? "")
                }
            }
            diagrammView?.dataArray = values
            diagrammView?.abscissaValues = keys
            diagrammView?.title = "Минимальная температура"
            diagrammView?.color = UIColor.blue
            diagrammView?.startColor = UIColor(red: 250.0 / 255.0, green: 233.0 / 255.0, blue: 222.0 / 255.0, alpha: 1.0)
            diagrammView?.endColor = UIColor(red: 252.0 / 255.0, green: 79.0 / 255.0, blue: 8.0 / 255.0, alpha: 1.0)

        }
    }
    
    func highTempButtonAction(_ sender: UIButton) {
        if let items = itemsSet {
            var values = Array<Double>()
            var keys = Array<String>()
            for (index, item) in items.enumerated() {
                if (index % 2) != 0 {
                    values.append(Double(item.high_temp))
                    keys.append(item.weekday_short ?? "")
                }
            }
            diagrammView?.dataArray = values
            diagrammView?.abscissaValues = keys
            diagrammView?.title = "Максимальная температура"
            diagrammView?.color = UIColor.red
            diagrammView?.startColor = UIColor(red: 222.0 / 255.0, green: 233.0 / 255.0, blue: 250.0 / 255.0, alpha: 1.0)
            diagrammView?.endColor = UIColor(red: 8.0 / 255.0, green: 79.0 / 255.0, blue: 252.0 / 255.0, alpha: 1.0)
        }
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        initializeIfNeeded()
        animator.addBehavior(diagrammBehavior)

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        animator.removeBehavior(diagrammBehavior)
    }
    
    private func initializeIfNeeded() {
        if diagrammView == nil {
            diagrammView = DiagrammaView()
            diagrammView.isOpaque = false
            view.addSubview(diagrammView)
            
            diagrammView.diagrammBehavior = diagrammBehavior
        }
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        diagrammView?.frame = view.bounds.divided(atDistance: view.bounds.height / 2, from: CGRectEdge.minYEdge).slice
        diagrammView?.frame = diagrammView?.frame.divided(atDistance: view.bounds.height / 2, from: CGRectEdge.minXEdge).slice ?? CGRect()
        diagrammView?.center = view.center
        if let dv = diagrammView {
            let bounds = CGRect(x: dv.frame.minX, y: dv.frame.maxY, width: dv.frame.width, height: 40.0)
            lowTempButton.frame = bounds.divided(atDistance: dv.frame.width / 2, from: CGRectEdge.minXEdge).slice
            highTempButton.frame = bounds.divided(atDistance: dv.frame.width / 2, from: CGRectEdge.minXEdge).remainder
        }
        diagrammBehavior.acceleration.angle = -CGFloat.pi / 2
        diagrammBehavior.acceleration.magnitude = 0.8
    }
}

