//
//  ViewController.swift
//  WeatherApplication
//
//  Created by Admin on 20.07.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData
import Kingfisher
import RxSwift

class ViewController: UIViewController, UITextFieldDelegate, CLLocationManagerDelegate {

    fileprivate let reuseIdentifier = "ForecastCollectionViewCell"
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 20.0, bottom: 10.0, right: 20.0)
    
    @IBOutlet weak var isOnLineLabel: UILabel!
    @IBOutlet weak var weatherImage: UIImageView!
    @IBOutlet weak var windDescLabel: UILabel!
    @IBOutlet weak var temperatureLabel: UILabel!
    @IBOutlet weak var weatherTypeLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var weekdayLabel: UILabel!
    @IBOutlet weak var spiner: UIActivityIndicatorView!
    @IBOutlet weak var searchTextField: UITextField! {
        didSet {
            searchTextField.delegate = self
        }
    }
    @IBOutlet weak var forecastCollectionView: UICollectionView!
    
    @IBAction func currentLocationButtonAction(_ sender: UIButton) {
        spiner.startAnimating()
        viewModel.startPolling.onNext(false)
    }
    
    var searchText: String? {
        didSet {
            searchTextField?.text = searchText
            searchTextField?.resignFirstResponder()
            if let text = searchText {
                if !text.isEmpty {
                    spiner.startAnimating()
                    viewModel.searchText.onNext(text)
                }
            }
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTextField {
            searchText = textField.text
        }
        return true
    }

    let disposeBag = DisposeBag()
    var viewModel = ViewModel()
  
    var location: Place?
    var currentWeather: Weather? {
        didSet {
            updateMainUI()
        }
    }

    var forecastArray = Array<HourlyForecastItem>() {
        didSet {
            forecastCollectionView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        forecastCollectionView.dataSource = self
        forecastCollectionView.delegate = self
        
        spiner.hidesWhenStopped = true
        
        viewModel.place.subscribe(onNext: { place in
            self.location = place
        }).addDisposableTo(disposeBag)
        
        viewModel.weather.subscribe(onNext: { weather in
            self.currentWeather = weather
        }).addDisposableTo(disposeBag)
 
        viewModel.dataReady.subscribe(onNext: { _ in
            self.loadDataFromDatabse()
        }).addDisposableTo(disposeBag)
        
        viewModel.isOnline.subscribe(onNext: { isOnline in
            switch isOnline {
            case true:
                self.isOnLineLabel.text = "Online"
                self.isOnLineLabel.textColor = UIColor.cyan
            case false:
                self.isOnLineLabel.text = "Offline"
                self.isOnLineLabel.textColor = UIColor.red
            }
        }).addDisposableTo(disposeBag)
        
        viewModel.errorBuffer.subscribe(onNext: { _ in
            DispatchQueue.main.async {
                self.spiner.stopAnimating()
            }
        }).addDisposableTo(disposeBag)
        
        view.addGradient()
    }
    
    private func updateMainUI() {
        if let location = location,
            locationLabel.text != location.localName?.uppercased() {
            locationLabel.text = location.localName?.uppercased()
        }
        if let weather = currentWeather {
            dateLabel.text = weather.date.uppercased()
            weekdayLabel.text = weather.weekday.uppercased()
            temperatureLabel.text = "\(Int(weather.temp_c) > 0 ? "+" : "")\(weather.temp_c)˚C"
            weatherTypeLabel.text = weather.weather_type
            windDescLabel.text = "Ветер \(weather.wind_kmh) км/час"
            if let url = URL(string: weather.icon_url) {
                weatherImage.kf.setImage(with: url)
            }
        }
    }
    
    private func loadDataFromDatabse() {
        DispatchQueue.main.async {
            var result: RequestResult
            if self.location == nil {
                result = DatabaseWorker.getPlace()
                switch result {
                case .success(let data):
                    if let place = data as? Place {
                        self.location = place
                    }
                case .failure(let error):
                    self.viewModel.errorBuffer.onNext(error)
                }
            }
            if self.currentWeather == nil {
                result = DatabaseWorker.getWeather()
                switch result {
                case .success(let data):
                    if let weather = data as? Weather {
                        self.currentWeather = weather
                    }
                case .failure(let error):
                    self.viewModel.errorBuffer.onNext(error)
                }
            }
            result = DatabaseWorker.getHourlyForecast()
            switch result {
            case .success(let data):
                if let array = data as? Array<HourlyForecastItem> {
                    self.forecastArray = array
                }
            case .failure(let error):
                self.viewModel.errorBuffer.onNext(error)
            }
            self.spiner.stopAnimating()
        }
    }
}

extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return forecastArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: reuseIdentifier, for: indexPath)
        
        if let cell = cell as? ForecastCollectionViewCell {
            cell.forecast = forecastArray[indexPath.row]
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let paddingSpace = sectionInsets.left * 4
        let availableWidth = collectionView.bounds.width - paddingSpace
        let widthPerItem = availableWidth / 3
        
        return CGSize(width: widthPerItem, height: collectionView.bounds.height - sectionInsets.top * 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

extension UIView {
    func addGradient() {
        let startColor = UIColor(red: 0.0 / 255.0, green: 114.0 / 255.0, blue: 140.0 / 255.0, alpha: 1.0)
        let endColor = UIColor(red: 52.0 / 255.0, green: 191.0 / 255.0, blue: 186.0 / 255.0, alpha: 1.0)
        let gradient = CAGradientLayer()
        gradient.frame = bounds
        gradient.colors = [startColor.cgColor, endColor.cgColor]
        layer.insertSublayer(gradient, at: 0)
    }
}
