//
//  StartUpViewController.swift
//  WeatherApplication
//
//  Created by Admin on 08.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit
import CoreLocation
import RxSwift

class StartUpViewController: UIViewController {

    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    private var viewModel: ViewModel = ViewModel()
    private let disposeBag: DisposeBag = DisposeBag()
    private var dataReadySubscription: Disposable?
    
    @IBAction func updateButtonTap(_ sender: UIButton) {
        dataReadySubscription?.dispose()
        viewModel.dataReady.onNext(false)
        dataReadySubscription = viewModel.dataReady.subscribe(onNext: self.dataReadyObserver(isNotInitial:))
        activityIndicator.startAnimating()
        self.viewModel.startPolling.onNext(true)
    }
    
    override func viewDidLoad() {
        updateButton.isHidden = true
        super.viewDidLoad()
        activityIndicator.hidesWhenStopped = true
        self.activityIndicator.startAnimating()
        viewModel.places.subscribe(onNext: getCurrentLocation).addDisposableTo(disposeBag)
        dataReadySubscription = viewModel.dataReady.subscribe(onNext: self.dataReadyObserver(isNotInitial:))
        viewModel.errorBuffer.subscribe(onNext: self.errorHandler(_:)).addDisposableTo(disposeBag)
        self.viewModel.startPolling.onNext(true)
    }
    
    private func dataReadyObserver(isNotInitial: Bool) {
        if isNotInitial {
            DispatchQueue.main.async {
                self.activityIndicator.stopAnimating()
                self.updateButton.isHidden = false
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                if let mainVC = mainStoryboard.instantiateViewController(withIdentifier: "MainView") as? ViewController {
                    mainVC.viewModel = self.viewModel
                    self.navigationController?.show(mainVC, sender: self)
                    self.dataReadySubscription?.dispose()
                }
            }
        }
    }
    
    private func getCurrentLocation(array: [Place]?) {
        if let places = array {
            if !places.isEmpty {
                if places.count > 1 {
                    guard
                        let nc = navigationController
                        else {
                            return
                    }
                    let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                    if let geoLookupController = mainStoryboard.instantiateViewController(withIdentifier: "GeoLookupTableView") as? GeoLookupTableViewController {
                        geoLookupController.locationsArray = places
                        geoLookupController.viewModel = viewModel
                        nc.show(geoLookupController, sender: self)
                    }
                } else {
                    viewModel.partiallyPlace.onNext(places[0])
                }
            }
        }
    }
    
    private func errorHandler(_ error: Error) {
        self.activityIndicator.stopAnimating()
        updateButton.isHidden = false
        if let error = error as? RequestError {
            requestErrorHandler(error)
        } else {
            showErrorView(withMessage: "Произошла непредвиденная ошибка.", withActionTitle: nil, withAction: nil)
        }
    }
    
    private func requestErrorHandler(_ error: RequestError) {
        switch error {
        case .locationIsProhibited:
            showErrorView(withMessage: "Запрещено определение местоположения. Внесите изменения в системные настройки.", withActionTitle: nil, withAction: nil)
        case .dataOlderThenExpected:
            showErrorView(withMessage: "Невозможно обновить данные в базе. Использовать необновленные данные?", withActionTitle: "Да", withAction: dataOlderThenExpectedCallBack)
        case .databaseError:
            showErrorView(withMessage: "Ошибка при обращении к базе данных.", withActionTitle: nil, withAction: nil)
        case .invalidJSONData:
            showErrorView(withMessage: "Непредвиденный формат данных.", withActionTitle: nil, withAction: nil)
        default:
            showErrorView(withMessage: "Произошла непредвиденная ошибка.", withActionTitle: nil, withAction: nil)
        }
    }
    
    private func showErrorView(withMessage message: String, withActionTitle title: String?, withAction action: (() -> Void)?) {
        guard
            let nc = navigationController
            else {
                return
        }
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        if let errorViewController = mainStoryboard.instantiateViewController(withIdentifier: "ErrorView") as? ErrorViewController {
            errorViewController.message = message
            if let title = title, let action = action {
                errorViewController.setAction(withTitle: title, action: action)
            }
            nc.show(errorViewController, sender: self)
        }
    }
    
    private func dataOlderThenExpectedCallBack() {
        self.viewModel.dataReady.onNext(true)
    }
}
