//
//  DailyForecastItem.swift
//  WeatherApplication
//
//  Created by Admin on 03.08.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class DailyForecastItem : NSManagedObject, Mappable {
    var icon_url: String {
        return WeatherApi.urlForImage(icon ?? "")
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        
    }
    
    func mappingTxt(map: Map) {
        let transformOfIntToInt32 = TransformOf<Int32, Int>(fromJSON: IntToInt32, toJSON: Int32ToJSON)
        icon <- (map["icon"])
        desc <- map["fcttext_metric"]
        weekday <- map["title"]
        period <- (map["period"], transformOfIntToInt32)
    }
    
    func mappingSimple(map: Map) {
        let transformOfIntToInt32 = TransformOf<Int32, Int>(fromJSON: IntToInt32, toJSON: Int32ToJSON)
        let transformOfStringToInt32 = TransformOf<Int32, String>(fromJSON: StringToInt32, toJSON: Int32toJSONString)
        
        high_temp <- (map["high.celsius"], transformOfStringToInt32)
        low_temp <- (map["low.celsius"], transformOfStringToInt32)
        monthname <- map["date.monthname"]
        month <- (map["date.month"], transformOfIntToInt32)
        year <- (map["date.year"], transformOfIntToInt32)
        day <- (map["date.day"], transformOfIntToInt32)
        weekday_short <- map["date.weekday_short"]
    }
    
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "dd.MM.yyyy"
        if let dt = dateFormatter.date(from: "\(day).\(month).\(year)") {
            dateFormatter.dateStyle = .long
            return dateFormatter.string(from: dt)
        } else {
            return String()
        }
    }
    
    var temperatureInterval: String {
        var result: String = ""
        if low_temp > 0 {
            result += "+\(low_temp)"
        } else {
            result += "\(low_temp)"
        }
        result += "..."
        if high_temp > 0 {
            result += "+\(high_temp)"
        } else {
            result += "\(high_temp)"
        }
        return "\(result)˚C"
    }
    
    private func Int32ToJSON(number: Int32?) -> Int? {
        return Int(number ?? 0)
    }
    
    private func IntToInt32(int: Int?) -> Int32? {
        return Int32(int ?? 0)
        
    }
    
    private func Int32toJSONString(number: Int32?) -> String {
        return String(number ?? 0)
    }
    
    private func StringToInt32(string: String?) -> Int32? {
        return Int32(Int(string ?? "") ?? 0)
    }
}
