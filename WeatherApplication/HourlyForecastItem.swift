//
//  HourlyForecastItem.swift
//  WeatherApplication
//
//  Created by Admin on 12.09.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class HourlyForecastItem : NSManagedObject, Mappable {
    
    var temp: String {
        if let t = Int(c_temp ?? "") {
            return "\(t >= 0 ? "+" : "")\(t)"
        }
        return c_temp ?? ""
    }
    var icon_url: String {
        return WeatherApi.urlForNightImage(icon ?? "", hour: hour)
    }
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        let transformOfStringToInt32 = TransformOf<Int32, String>(fromJSON: StringToInt32, toJSON: Int32toJSONString)
        
        c_temp <- map["temp.metric"]
        condition  <- map["condition"]
        icon <- map["icon"]
        wspd <- map["wspd.metric"]
        wdir <- map["wdir.dir"]
        humidity <- map["humidity"]
        feelslike <- map["feelslike.metric"]
        mslp <- map["mslp.metric"]
        hour <- (map["FCTTIME.hour"], transformOfStringToInt32)
        min <- (map["FCTTIME.min"], transformOfStringToInt32)
        year <- (map["FCTTIME.year"], transformOfStringToInt32)
        yday <- (map["FCTTIME.yday"], transformOfStringToInt32)
        weekday_name_abbrev <- map["FCTTIME.weekday_name_abbrev"]
    }
    
    var date: String {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "HH:mm"
        if let dt = dateFormatter.date(from: "\(hour):\(min)") {
            return dateFormatter.string(from: dt)
        } else {
            return ""
        }
    }
    
    private func Int32toJSONString(number: Int32?) -> String {
        return String(number ?? 0)
    }
    
    private func StringToInt32(string: String?) -> Int32? {
        return Int32(Int(string ?? "") ?? 0)
    }

}
