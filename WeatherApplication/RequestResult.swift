//
//  RequestResult.swift
//  WeatherApplication
//
//  Created by Admin on 08.10.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import Foundation

enum RequestResult {
    case success(Any)
    case failure(Error)
}

enum RequestError: Error {
    case invalidJSONData
    case unexpectedError
    case databaseError
    case dataOlderThenExpected
    case locationIsProhibited
    case isNotConnectedToNetwork
}
