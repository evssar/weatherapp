//
//  DiagrammPointView.swift
//  CoreGraphicApplication
//
//  Created by Admin on 10.09.17.
//  Copyright © 2017 Admin. All rights reserved.
//

import UIKit

class DiagrammPointView: UIView {

    var lineWidth: CGFloat = 0.0 { didSet { setNeedsDisplay() }}
    var strokeColor: UIColor = UIColor.blue { didSet { setNeedsDisplay() }}
    var fillColor: UIColor = UIColor.red { didSet { setNeedsDisplay() }}
    
    override func draw(_ rect: CGRect) {
        let path: UIBezierPath = UIBezierPath(ovalIn: bounds.insetBy(dx: lineWidth / 2, dy: lineWidth / 2))
        path.lineWidth = lineWidth
        strokeColor.setStroke()
        fillColor.setFill()
        path.stroke()
        path.fill() 
    }
}
